# +----------------------------------------------------------------------
# | DjangoAdmin敏捷开发框架 [ 赋能开发者，助力企业发展 ]
# +----------------------------------------------------------------------
# | 版权所有 2021~2025 北京DjangoAdmin研发中心
# +----------------------------------------------------------------------
# | Licensed Apache-2.0 DjangoAdmin并不是自由软件，未经许可禁止去掉相关版权
# +----------------------------------------------------------------------
# | 官方网站: https://www.djangoadmin.cn
# +----------------------------------------------------------------------
# | 作者: @一米阳光 团队荣誉出品
# +----------------------------------------------------------------------
# | 版权和免责声明:
# | 本团队对该软件框架产品拥有知识产权（包括但不限于商标权、专利权、著作权、商业秘密等）
# | 均受到相关法律法规的保护，任何个人、组织和单位不得在未经本团队书面授权的情况下对所授权
# | 软件框架产品本身申请相关的知识产权，禁止用于任何违法、侵害他人合法权益等恶意的行为，禁
# | 止用于任何违反我国法律法规的一切项目研发，任何个人、组织和单位用于项目研发而产生的任何
# | 意外、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、
# | 附带或衍生的损失等)，本团队不承担任何法律责任，本软件框架禁止任何单位和个人、组织用于
# | 任何违法、侵害他人合法利益等恶意的行为，如有发现违规、违法的犯罪行为，本团队将无条件配
# | 合公安机关调查取证同时保留一切以法律手段起诉的权利，本软件框架只能用于公司和个人内部的
# | 法律所允许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
# +----------------------------------------------------------------------

from django.shortcuts import render

# Create your views here.

from django.utils.decorators import method_decorator
from django.views import View

from application.city import services
from application.constants import CITY_LEVEL_LIST
from config.env import DJANGO_DEMO
from middleware.login_middleware import check_login
from middleware.permission_middleware import PermissionRequired

from utils import R


# 渲染城市首页
@method_decorator(check_login, name='get')
class CityView(PermissionRequired, View):
    # 方法权限标识
    permission_required = ('sys:city:index',)

    # 接收GET请求
    def get(self, request):
        # 以下代码刷数据使用，可以直接删除
        # provinceList = City2.objects.filter(level_code=0).values()
        # for province in provinceList:
        #     pCity = City.objects.create(
        #         name=province['name'],
        #         city_code=province['city_code'],
        #         area_code=province['area_code'],
        #         parent_code=province['parent_code'],
        #         level=province['level_code'] + 1,
        #         zip_code=province['zip_code'],
        #         short_name=province['short_name'],
        #         full_name=province['merger_name'],
        #         pinyin=province['pinyin'],
        #         lng=province['lng'],
        #         lat=province['lat'],
        #         pid=0,
        #     )
        #     # 查询城市列表
        #     cityList = City2.objects.filter(level_code=1, parent_code=province['area_code']).values()
        #     for city in cityList:
        #         cCity = City.objects.create(
        #             name=city['name'],
        #             city_code=city['city_code'],
        #             area_code=city['area_code'],
        #             parent_code=city['parent_code'],
        #             level=city['level_code'] + 1,
        #             zip_code=city['zip_code'],
        #             short_name=city['short_name'],
        #             full_name=city['merger_name'],
        #             pinyin=city['pinyin'],
        #             lng=city['lng'],
        #             lat=city['lat'],
        #             pid=pCity.id,
        #         )
        #         # 查询县区列表
        #         areaList = City2.objects.filter(level_code=2, parent_code=city['area_code']).values()
        #         for area in areaList:
        #             dCity = City.objects.create(
        #                 name=area['name'],
        #                 city_code=area['city_code'],
        #                 area_code=area['area_code'],
        #                 parent_code=area['parent_code'],
        #                 level=area['level_code'] + 1,
        #                 zip_code=area['zip_code'],
        #                 short_name=area['short_name'],
        #                 full_name=area['merger_name'],
        #                 pinyin=area['pinyin'],
        #                 lng=area['lng'],
        #                 lat=area['lat'],
        #                 pid=cCity.id,
        #             )

        # 渲染HTML模板
        return render(request, "city/index.html")


# 查询城市分页数据
@method_decorator(check_login, name='get')
class CityListView(PermissionRequired, View):
    # 方法权限标识
    permission_required = ('sys:city:list',)

    # 接收GET请求
    def get(self, request):
        # 调用查询城市分页数据
        result = services.CityList(request)
        # 返回结果
        return result


# 查询城市详情
@method_decorator(check_login, name='get')
class CityDetailView(PermissionRequired, View):
    # 方法权限标识
    permission_required = ('sys:city:detail',)

    # 接收GET请求
    def get(self, request, city_id):
        # 调用查询城市详情方法
        data = services.CityDetail(city_id)
        # 模板参数
        content = {
            'levelList': CITY_LEVEL_LIST,
            'data': data
        }
        # 渲染模板并绑定参数
        return render(request, 'city/edit.html', content)


# 添加城市
@method_decorator(check_login, name='post')
class CityAddView(PermissionRequired, View):
    # 方法权限标识
    permission_required = ('sys:city:add',)

    # 接收POST请求
    def post(self, request):
        if DJANGO_DEMO:
            return R.failed("演示环境，暂无操作权限")
        # 调用添加城市方法
        result = services.CityAdd(request)
        # 返回结果
        return result


# 更新城市
@method_decorator(check_login, name='put')
class CityUpdateView(PermissionRequired, View):
    # 方法权限标识
    permission_required = ('sys:city:update',)

    # 接收PUT请求
    def put(self, request):
        if DJANGO_DEMO:
            return R.failed("演示环境，暂无操作权限")
        # 调用更新城市方法
        result = services.CityUpdate(request)
        # 返回结果
        return result


# 删除城市
@method_decorator(check_login, name='delete')
class CityDeleteView(PermissionRequired, View):
    # 方法权限标识
    permission_required = ('sys:city:delete',)

    # 接收delete请求
    def delete(self, request, city_id):
        if DJANGO_DEMO:
            return R.failed("演示环境，暂无操作权限")
        # 调用删除城市方法
        result = services.CityDelete(city_id)
        # 返回结果
        return result


# 根据上级ID获取子级城市
@method_decorator(check_login, name='get')
class CityChildView(PermissionRequired, View):
    # 方法权限标识
    permission_required = ('sys:city:child',)

    # 接收GET请求
    def get(self, request, city_code):
        # 调用查询城市详情方法
        data = services.getChildList(city_code)
        # 返回结果
        return R.ok(data)
